''' 
    Michael Rogers
    Katherine
    3/30/2015
    ISTA 350
    HW#7
    This program is the beginning of the web scraping process and will be able
    to retrieve data from a website. 
'''
from bs4 import BeautifulSoup
import requests
import os
import zlib
import numpy as np


def get_soup(url=None, fname=None, gzipped=False):
    '''This function has three parameters, a url address passed in as a String 
    a default of None, a file name passed in as String with a default of None, 
    and isGzipped, which is a boolean value with a default of false, unless the
    the html to be parsed is gzipped. If the file is not empty, its contents 
    be passed to the be BeautifulSoup constructor and returns the object. If 
    the url is None, a RuntimeError will be raised. If the content is zipped, 
    which is indicated by our third parameter, it will be unzipped and passed 
    to our BeautifulSoup constructor, and the the resulting object returned.'''
    if fname is not None:
        file_data = open(fname, 'r').read()
        soup=BeautifulSoup(file_data)
        return soup
    elif url is None:
        raise RuntimeError('Either url or filename must be specified.')
    else:
        if gzipped:
            r = requests.get(url)
            content= zlib.decompress(r.content, 16+zlib.MAX_WBITS)
            soup=BeautifulSoup(content)
            return soup
        r = requests.get(url)
        url_data=r.text
        soup=BeautifulSoup(url_data)
        return soup
    
def save_soup(fname, soupObj):
    '''The function takes two arguments, a file name passed as a String, and a 
    soup object. A textual representation of the soup object in the file .'''
    #open file
    open_file=open(fname, 'w')
    #save soup text
    open_file.write(str(soupObj))
    #close file
    open_file.close
    
def scrape_and_save():
    '''This function scrapes three addresses and stores a textual 
    representation of these into our html file in the directory.''' 
    pcpn_url="http://www.wrcc.dri.edu/WRCCWrappers.py?sodxtrmts+028815+por+por+pcpn+none+msum+5+01+F"
    pcpn_soup=get_soup(url=pcpn_url)
    save_soup('wrcc_pcpn.html', pcpn_soup)
    
    mint_url = "http://www.wrcc.dri.edu/WRCCWrappers.py?sodxtrmts+028815+por+por+mint+none+mave+5+01+F"
    mint_soup=get_soup(url=mint_url)
    save_soup('wrcc_mint.html', mint_soup)
    
    maxt_url = "http://www.wrcc.dri.edu/WRCCWrappers.py?sodxtrmts+028815+por+por+maxt+none+mave+5+01+F"
    maxt_soup=get_soup(url=maxt_url)
    save_soup('wrcc_maxt.html', maxt_soup)
    
def is_num(str):
    '''This Boolean function takes a string, str, and determines whether or not
    it represents a number(int or float)'''
    neg=0
    per=0
    if str is '':
        return False
    i=0
    for num in str:        
        if num not in ['1','2','3','4','5','6','7','8','9','0','.','-']:
            return False
        if num == '.':
            per+=1
            if per==2:
                return False
        if num == '-' and i!=0:
            return False
        i+=1
    return True            
    
def load_lists(soup, flag):
    '''This function takes a soup object, soup, and a flag as arguments and 
    returns a list of lists containing the useful data in our soup object. The
    soup object contains an html parse tree that describes a table of data. The
    data will be extracted and stored in our list of lists. In the process, the
    data will be transposed so that the columns in the table are rows in the 
    list and vice versa. The first row should be a year. When you reach a row
    that doesn't have a year as its first datum, all of the row data has been
    gathered. Check data using is_num. If the table data field is '-----', 
    place the flag in the list of lists in its place. 
    '''
    
    lists = [[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
    first = True  # this called a flag
    
    for tr in soup.find_all('tr'):
        i=0
        for td in tr.find_all('td'):
            data_str = td.get_text()
            if is_num(data_str)==False and i==0 and first==False:
                return lists
            elif data_str =='-----':
                lists[i].append(flag)
                i+=1  
            elif is_num(data_str) and i==0:
                data_str=int(data_str)
                lists[i].append(data_str)
                i+=1
                first=False
                
            elif is_num(data_str):
                data_str=float(data_str)
                lists[i].append(data_str)
                i+=1
    return lists

def replace_na(lst, x, y, flag):
    '''This function replaces the element data[r][c], with the average of the
    surrounding 10 years. Its first parameter is the list, the second and 
    third parameter are row and column respectively, and the final element is a
    flag. If one of the surrounding years also contains the flag, the element 
    in that position will be left out of the average.'''
    sum_list=[]
    i=1
    j=1
    
    #Go 5 positions left
    while i <= 5:
        if y - i >=0:
            if lst[x][y-i]!=flag:
                sum_list.append(lst[x][y-i])
            i+=1
        else:
            break
    #Go 5 positions right
    while j <=5:
        if y+j< len(lst[x]):
            if lst[x][y+j]!=flag:
                sum_list.append(lst[x][y+j])
            j+=1
        else:
            break
        
    return sum(sum_list)/len(sum_list)
    
def clean_data(lst, flag):
    '''This function traverses the list of lists and ever time it finds the
    flag, it calls replace_na to replace the flag. Its parameters are the list 
    and the flag.'''
    for x in range(len(lst)):
        for y in range(len(lst[x])):
            if lst[x][y] == flag:
                lst[x][y]=replace_na(lst, x, y, flag)
 
def recalculate_annual_data(lsts, average=False):
    '''The last list in our list of lists contains the annual average for each
    year in the case of temperature data/the annual total in the case of 
    precipitation. '''
    x=0
    y=0
    while y< len(lsts[0]):
        first = True
        curr_list=[]
        x=0
        while x < len(lsts):
            if not first:
                curr_list.append(lsts[x][y])
                x+=1
            else: 
                first= False
                x+=1
            if x == len(lsts)-1:
                if average:
                    lsts[len(lsts)-1][y]=sum(curr_list)/len(curr_list)
                else:
                    lsts[len(lsts)-1][y]=sum(curr_list)
        y+=1

def clean_and_jsonify(fname_list, flag):
    ''' This function takes two arguments, the first a list of file names, 
    fname_list, and a flag, flag. for each file in the list, get soup, 
    transform into a list of lists, clean the list, recalculate the annual data
    and store it in a file as a json object.'''
    for fname in fname_list:
        soup=get_soup(fname=fname)
        lst_of_lsts=load_lists(soup, flag)
        clean_data(lst_of_lsts, flag)
        average ='pcpn' in fname:
        recalculate_annual_data(lst_of_lsts, average)
        
        


def main():
    '''Checks the current directory for any on of the files that 
    scrape_and_save creates. If it is not there, save the addresses and print 
    a message notifying the user that this is occurring. '''
    if not os.path.isfile('wrcc_pcpn.html') or not os.path.isfile('wrcc_mint.html') or not os.path.isfile('wrcc_maxt.html'):
        print( '---- scraping and saving ----')
        scrape_and_save()
    fnames= ['wrcc_pcpn.html', 'wrcc_mint.html', 'wrcc_maxt.html']
    clean_and_jsonify(fnames, -999)
    